@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Models/ContactList {{ $models/contactlist->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('models/-contact-list/' . $models/contactlist->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Models/ContactList"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['models/contactlist', $models/contactlist->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Models/ContactList',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $models/contactlist->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $models/contactlist->name }} </td></tr><tr><th> User Id </th><td> {{ $models/contactlist->user_id }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection