@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Images for <b>{{$ad->title}}</b> Advert Campaign </div>
                    <div class="panel-body">

                        <a href="{{ url('/ads/'.$ad->id.'/ad-images/create') }}" class="btn btn-primary btn-xs" title="Add New AdImage"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th><th> Ad Id </th><th> Reference </th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($adimages as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->ad_id }}</td>
                                        <td><img width="50"  src=<?php echo env("APP_URL").'/ad_images/'.md5($item->ad_id).'/'.$item->reference;?>></td>
                                        <td>
                                            <a href="{{ url('/ad-images/' . $item->id) }}" class="btn btn-success btn-xs" title="View AdImage"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url('/ad-images/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit AdImage"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/ad-images', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete AdImage" />', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete AdImage',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $adimages->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection