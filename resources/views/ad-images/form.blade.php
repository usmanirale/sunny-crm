<div class="form-group {{ $errors->has('ad_id') ? 'has-error' : ''}}">
    {{-- {!! Form::label('ad_id', 'Ad Id', ['class' => 'col-md-4 control-label']) !!} --}}
    <div class="col-md-6">
        {!! Form::hidden('ad_id', $ad->id, ['class' => 'form-control']) !!}
        {!! $errors->first('ad_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
    {!! Form::label('reference', 'Image', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('reference', null, ['class' => 'form-control']) !!}
        {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>