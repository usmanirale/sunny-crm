@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">SentMessage {{ $sentmessage->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('sent-message/' . $sentmessage->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit SentMessage"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['sentmessage', $sentmessage->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete SentMessage',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $sentmessage->id }}</td>
                                    </tr>
                                    <tr><th> Message Id </th><td> {{ $sentmessage->message_id }} </td></tr><tr><th> Contact List Id </th><td> {{ $sentmessage->contact_list_id }} </td></tr><tr><th> Sent To All </th><td> {{ $sentmessage->sent_to_all }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection