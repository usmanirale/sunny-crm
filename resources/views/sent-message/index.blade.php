@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Sentmessage</div>
                    <div class="panel-body">

                        <a href="{{ url('/sent-message/create') }}" class="btn btn-primary btn-xs" title="Add New SentMessage"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th><th> Message  </th><th> Contact List id </th><th> Sent To All </th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($sentmessage as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->message->title }}</td><td>{{ $item->contact_list_id }}</td><td>{{ $item->sent_to_all }}</td>
                                        <td>
                                            <a href="{{ url('/sent-message/' . $item->id) }}" class="btn btn-success btn-xs" title="View SentMessage"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url('/sent-message/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit SentMessage"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/sent-message', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete SentMessage" />', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete SentMessage',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $sentmessage->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection