<div class="form-group {{ $errors->has('message_id') ? 'has-error' : ''}}">
    {!! Form::label('message_id', 'Messages', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {{-- {!! Form::number('message_id', null, ['class' => 'form-control']) !!} --}}
        {!! Form::select('message_id', $messages, ['class' => 'form-control']) !!}
        {!! $errors->first('message_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('contact_list_id') ? 'has-error' : ''}}">
    {!! Form::label('contact_list_id', 'Contact Lists', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {{-- {!! Form::number('contact_list_id', null, ['class' => 'form-control']) !!} --}}
        {!! Form::select('contact_list_id', $contact_lists, ['class' => 'form-control']) !!}
        {!! $errors->first('contact_list_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('sent_to_all') ? 'has-error' : ''}}">
    {!! Form::label('sent_to_all', 'Sent To All', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {{-- {!! Form::text('sent_to_all', null, ['class' => 'form-control']) !!} --}}
        {!! Form::select('sent_to_all', $select_all, ['class' => 'form-control']) !!}
        {!! $errors->first('sent_to_all', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>