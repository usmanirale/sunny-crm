<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SentMessage;
use Illuminate\Http\Request;
use Session;

use App\Models\Message;
use App\Models\Contact;
use App\Models\ContactList;
use App\Models\Ad;
use App\Mail\newsLetter;
use Illuminate\Support\Facades\Mail;

class SentMessageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $sentmessage = SentMessage::with('message')->with('contactlist')->paginate(25);
        return view('sent-message.index', compact('sentmessage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $messages = Message::pluck('title', 'id')->all();
        $contact_lists = ContactList::pluck('name', 'id')->all();
        $select_all = [
                        ['name' => 'No', 'value' => 'No'],
                        ['Name' => 'Yes', 'value' => 'Yes']
                    ];
        return view('sent-message.create', compact('messages', 'contact_lists', 'select_all'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        $save = SentMessage::create($requestData);
        // dd($save);
        $contacts =  Contact::where('contact_list_id', $save->contact_list_id)->get();
        $message = Message::find($save->message_id);
        // dd($message->body);
        $servedAd = (new Ad)->serveAds();
        // dd($servedAd);
        Mail::to($contacts)->send(new newsLetter($servedAd, $message));

        Session::flash('flash_message', 'SentMessage added!');

        return redirect('sent-message');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $sentmessage = SentMessage::findOrFail($id);

        return view('sent-message.show', compact('sentmessage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $sentmessage = SentMessage::findOrFail($id);

        return view('sent-message.edit', compact('sentmessage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $sentmessage = SentMessage::findOrFail($id);
        $sentmessage->update($requestData);

        Session::flash('flash_message', 'SentMessage updated!');

        return redirect('sent-message');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        SentMessage::destroy($id);

        Session::flash('flash_message', 'SentMessage deleted!');

        return redirect('sent-message');
    }
}
