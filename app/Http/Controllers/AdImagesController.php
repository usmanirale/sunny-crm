<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Ad;
use App\Models\AdImage;
use Illuminate\Http\Request;
use Session;
use Intervention\Image\ImageManager;


class AdImagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->manager = new ImageManager();

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index($ad_id)
    {
        $ad = Ad::find($ad_id);
        $adimages = AdImage::paginate(25);

        return view('ad-images.index', compact('adimages', 'ad'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($ad_id)
    {
        $ad = Ad::find($ad_id);
        return view('ad-images.create', compact('ad'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        if($request->hasFile('reference')){

            $main_dir = env("AD_IMAGES");
            $upload_dir = $this->getUploadDir($request->ad_id, $main_dir);

            $extension = $request->reference->extension();
            $filename = uniqid().$request->ad_id.'.'.$extension;
            $new_filename = $upload_dir.'/'. $filename;

            $upload = $this->upload($request->reference, $new_filename, '');
        }

        AdImage::create(['ad_id' => $request->ad_id, 'reference' => $filename]);

        Session::flash('flash_message', 'AdImage added!');

        return redirect("/ads");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $adimage = AdImage::findOrFail($id);

        return view('ad-images.show', compact('adimage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $adimage = AdImage::findOrFail($id);

        return view('ad-images.edit', compact('adimage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $adimage = AdImage::findOrFail($id);
        $adimage->update($requestData);

        Session::flash('flash_message', 'AdImage updated!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        AdImage::destroy($id);

        Session::flash('flash_message', 'AdImage deleted!');

        return redirect()->back();
    }

    private function upload($filepath = '', $newfile ='', $watermark_image = '')
    {
        $file = $this->manager->make($filepath)/*->resize(445, 458)*/;

        if($watermark_image != '')
            $file->insert($watermark_image, 'bottom-right', 10, 10);

        if($file->save($newfile))
            return true;
        return false;
    }

    public function getUploadDir($id, $path)
    {
        $dir_name = md5($id);
        $dir_path = $path.$dir_name;

        if (!file_exists($dir_path)) {
          mkdir($dir_path, 0777, true);
        }
        return $dir_path;
    }
}
