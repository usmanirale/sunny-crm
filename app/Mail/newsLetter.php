<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Ad;
use App\Models\Message;

class newsLetter extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Ad $ad, Message $message)
    {
        $this->ads = $ad;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.plain')
                    ->from('info@test.com', 'Marketing Email')
                    ->subject($this->message->title)
                    ->with([
                        'body' => $this->message->body,
                        'ad_image_reference' => $this->ads->advertImages[0]->reference,
                        'ad_id' => $this->ads->id,
                        'ad_catch_phrase' => $this->ads->catch_phrase,
                        ]);
    }
}
