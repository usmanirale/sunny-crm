<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactList extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact_lists';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_id'];

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact');
    }
    public function messages()
    {
        return $this->belongsTo('App\Models\Message');
    }
    public function sentmessage()
    {
        return $this->hasMany('App\Models\SentMessage');
    }

}
