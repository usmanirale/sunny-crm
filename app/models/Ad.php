<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ads';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'catch_phrase'];

    public function serveAds()
    {
        $selcted_ads = [];

        $ads = $this->with('advertImages')->get();
        $ads_count = $ads->count();

        $random_ads_id = rand(0, $ads_count);

        if($ads_count > 0)
            $selcted_ads = $ads[0];

        return $selcted_ads;

    }

    public function advertImages()
    {
        return $this->hasMany('App\Models\AdImage');
    }


}
