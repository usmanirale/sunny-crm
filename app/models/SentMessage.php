<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SentMessage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sent_messages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['message_id', 'contact_list_id', 'sent_to_all'];


    public function message()
    {
        return $this->belongsTo('App\Models\Message');
    }

    public function contactlist()
    {
        return $this->belongsTo('App\Models\ContactList');
    }


}
