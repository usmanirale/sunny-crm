<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('user-group', 'UserGroupController');
Route::resource('contact-list', 'ContactListController');
Route::resource('contact', 'ContactController');
Route::resource('message', 'MessageController');
Route::resource('message-type', 'MessageTypeController');
Route::resource('sent-message', 'SentMessageController');
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('ads', 'AdsController');
Route::resource('ad-images', 'AdImagesController');

Route::match(['POST', 'GET'], '/ads/{ad_id}/ad-images', 'AdImagesController@index');
Route::match(['POST', 'GET'], '/ads/{ad_id}/ad-images/create', 'AdImagesController@create');